﻿using System;
using System.Collections.Generic;

namespace Services
{
    public class ProductRepository
    {
        static readonly Product[] _products = new Func<Product[]>(() =>
        {
            var products = new Product[10];

            for (var i = 0; i < products.Length; i++)
            {
                products[i] = new Product
                {
                    Title = $"Product#{i}",
                    Description = $"This is the #{i} product.",
                    Reference = i,
                    Price = 1000.0M * i
                };
            }

            return products;
        })();

        public IEnumerable<Product> GetAll() => _products;

        public Product GetProductWithOutOfSeason()
        {
            var product = new Product
            {
                Title = "Product#999",
                Description = "This is the #999 product.",
                Reference = 999,
                Price = 1000.0M
            };
            var policy = GetOutOfSeasonPolicy();

            product.Policies.Add(policy);

            return product;
        }

        public ProductPolicy GetOutOfSeasonPolicy() => new ProductPolicy
        {
            Description = "Out of season",
            Modifier = new AbsoluteModifier
            {
                Amount = 10
            }
        };
    }
}
