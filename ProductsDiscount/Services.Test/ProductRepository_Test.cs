﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Services.Test
{
    [TestClass]
    public class ProductRepository_Test
    {
        [TestMethod]
        public void GetAll()
        {
            var sut = new ProductRepository();
            var products = sut.GetAll().ToArray();

            Assert.AreEqual(10, products.Length);

            for (var i = 0; i < products.Length; i++)
            {
                var product = products[i];

                Assert.AreEqual($"Product#{i}", product.Title);
                Assert.AreEqual($"This is the #{i} product.", product.Description);
                Assert.AreEqual(i, product.Reference);
                Assert.AreEqual(1000.0M * i, product.Price);
            }
        }

        [TestMethod]
        public void MakeProductOutOfSeason()
        {
            var sut = new ProductRepository();
            var product = sut.GetProductWithOutOfSeason();
            var expectedFinalPrice = product.Price + 10M;
            var finalPrice = product.GetFinalPrice();

            Assert.AreEqual(expectedFinalPrice, finalPrice);
        }
    }
}
