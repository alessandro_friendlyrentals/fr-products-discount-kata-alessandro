﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class Product
    {
        public int Reference { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public IList<ProductPolicy> Policies { get; } = new List<ProductPolicy>();

        public override string ToString() => $"{Title} - {Description}";

        public decimal GetFinalPrice()
        {
            var price = Price;

            foreach (var policy in Policies)
            {
                if (policy.Modifier is AbsoluteModifier absolute)
                {
                    price = price + absolute.Amount;
                }
                else if (policy.Modifier is PercentageModifier percentage)
                {
                    price = price * percentage.Amount;
                }
            }

            return price;
        }
    }
}
