﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Services.Abstractions")]
[assembly: AssemblyProduct("Services.Abstractions")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
