﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public abstract class PriceModifier
    {
        public decimal Amount { get; set; }
    }

    public class AbsoluteModifier : PriceModifier { }

    public class PercentageModifier : PriceModifier { }
}
