﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ProductPolicy
    {
        public string Description { get; set; }

        public PriceModifier Modifier { get; set; }
    }
}
