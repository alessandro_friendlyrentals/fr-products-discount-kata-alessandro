# Products discount kata #
  
This kata is designed to work on the project organization and design patterns.  
  
### Set up ###
  
Fork this repository and add "_" and your name as a suffix  
  
We suggest to use a TDD aproach.  
Do a commit after each test, implementation or refactor you do.   
  
You don't need to really persist the information in this kata  
The presentation is not required as well.  
Just take care of testing and implementing the following business rules and organize the code to be easy to understand and maintain in the future.  
That means, only implement the business layer with the services and the minimun persistence interfaces to test.
  
Implement thinking only on the sunshine path but thinking were the validations should take place in the future.  
Time between commits will be taken into account so hurry up!    
  
Share the final result with me  
:-)  
  
### Kata description ###
  
We need to build a landing page for the products of our on-line shop.  
The products has a reference, a title, a description, and a price.  
  
1. In our first version we just need to show all the products with the information.  
  
2. We realize some products are hard to sell when they are a few time on the market so we decide to mark some of them as "Out of season".  
All these products should apply 10€ discount on the final price.  
  
3. Now, our Marketing director says we need to push some products so we decide to mark some of the as "Special offer" so the prices decreases a 30% on these products.  
If a product has an special offer and is out of season the discount to apply should be the special offer.  





